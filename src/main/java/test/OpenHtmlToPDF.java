package test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.openhtmltopdf.extend.FSSupplier;
import com.openhtmltopdf.outputdevice.helper.BaseRendererBuilder;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

public class OpenHtmlToPDF {
    
    public static void createPdfByPostUrl(String postUrl, String baseUrl, Map<String, String> paramMap, String outputPdf) throws Exception {
        OutputStream out = null;
        try {
            String html = getHtmlByPost(postUrl, paramMap);
            out = new FileOutputStream(outputPdf);
            createPdfStream(html, baseUrl, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    public static void createPdfStreamByPostUrl(String postUrl, String baseUrl, Map<String, String> paramMap, ByteArrayOutputStream outStream) throws Exception {
        String html = getHtmlByPost(postUrl, paramMap);
        createPdfStream(html, baseUrl, outStream);
    }
    
    public static void createPdfByHtml(String html, String folder, String outputPdf) throws Exception {
        OutputStream out = null;
        try {
            out = new FileOutputStream(outputPdf);
            createPdfStream(html, folder, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    public static void createPdfStreamByHtml(String html, String folder, ByteArrayOutputStream outStream) throws Exception {
        createPdfStream(html, folder, outStream);
    }
    
    public static void createPdfByUrl(String htmlUrl, String baseUrl, String outputPdf) throws Exception {
        OutputStream out = null;
        try {
            out = new FileOutputStream(outputPdf);
            String html = getHtmlByGet(htmlUrl);
            createPdfStream(html, baseUrl, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    public static void createPdfStreamByUrl(String htmlUrl, String baseUrl, ByteArrayOutputStream outStream) throws Exception {
        String html = getHtmlByGet(htmlUrl);
        createPdfStream(html, baseUrl, outStream);
    }
    
    private static void createPdfStream(final String content, final String baseUrl, final OutputStream outputStream) throws Exception {
        
        PdfRendererBuilder builder = new PdfRendererBuilder();

        builder.useFont(new FSSupplier<InputStream>() {
            @Override
            public InputStream supply() {
                return OpenHtmlToPDF.class.getClassLoader().getResourceAsStream("cwTeXKai-zhonly.ttf");
            }
        }, "cwTeXKai-zhonly", 400, BaseRendererBuilder.FontStyle.NORMAL, true);
        
        builder.useFont(new FSSupplier<InputStream>() {
            @Override
            public InputStream supply() {
                return OpenHtmlToPDF.class.getClassLoader().getResourceAsStream("kaiu.ttf");
            }
        }, "kaiu", 400, BaseRendererBuilder.FontStyle.NORMAL, true);

        builder.withHtmlContent(content, baseUrl);
        
        builder.toStream(outputStream);

        builder.run();
    }
    
    public static String readResourceHtml(String resource) throws IOException {
        InputStream in = OpenHtmlToPDF.class.getClassLoader().getResourceAsStream(resource);
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF8"));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) sb.append(line + "\n");
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
    
    public static String getHtmlByGet(String url) throws IOException
    {
        URL urlObject = new URL(url);
        URLConnection urlConnection = urlObject.openConnection();
        urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        return toString(urlConnection.getInputStream());
    }

    private static String toString(InputStream inputStream) throws IOException
    {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8")))
        {
            String inputLine;
            StringBuilder stringBuilder = new StringBuilder();
            while ((inputLine = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(inputLine);
            }

            return stringBuilder.toString();
        }
    }
    
    public static String getHtmlByPost(String url, Map<String, String> postReq) throws Exception {

        HttpPost post = new HttpPost(url);

        // add request parameter, form parameters
        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        for (Entry<String, String> entry : postReq.entrySet()) {
            urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));;
        }
        
        post.setEntity(new UrlEncodedFormEntity(urlParameters));
        
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(post);
            return EntityUtils.toString(response.getEntity());
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }

    }
    
    public static void main(String[] args) throws Exception {
        
        // output file
        String outPdf = "E:/test.pdf";
        
        // html source
        //String html = readResourceHtml("test2.html");
        
        // base url to hold image, css files
        //String baseUrl = OpenHtmlToPDF.class.getClassLoader().getResource("").toString();
        //createPdfByHtml(html, baseUrl, outPdf);
        
        
        //String htmlUrl = "http://www.google.com.tw/";
        //String baseUrl = "http://www.google.com.tw/";
        //createPdfByUrl(htmlUrl, baseUrl, outPdf);
        
        
        Map<String, String> param = new HashMap<String, String>();
        param.put("queryCmd", "reset");
        String postUrl = "http://localhost:8080/test/test.do";
        String baseUrl = "http://localhost:8080/test/";
        createPdfByPostUrl(postUrl, baseUrl, param, outPdf);
        
        
    }
    
}